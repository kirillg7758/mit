import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {BlockchainService, IData} from "./blockchain.service";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-metamask',
  templateUrl: './metamask.component.html',
  styleUrls: ['./metamask.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MetamaskComponent implements OnInit, OnDestroy {
  blockchainData!: IData;

  private destroy$ = new Subject();

  constructor(
    public blockchainService: BlockchainService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.blockchainService
      .observable
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
      if (data !== null) {
        this.blockchainData = data;
        this.changeDetectorRef.detectChanges();
      }
    })
  }

  connect(): void {
    this.blockchainService.connectEthereum();
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}
